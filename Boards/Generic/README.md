# Custom-made elements

## Author

These chess boards and pieces were downloaded from [http://www.openclipart.org/] and pimped by Yannick Kirschhoffer <alcibiade@alcibiade.org>

They can be used without restriction.
