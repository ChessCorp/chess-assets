#!/bin/bash
rm -f ChessSet_01_*.png Pieces_*.png
convert -crop 180x190 ChessSet_01.png Pieces_%03d.png
convert -trim Pieces_000.png ChessSet_01_bk.png
convert -trim Pieces_001.png ChessSet_01_bq.png
convert -trim Pieces_002.png ChessSet_01_br.png
convert -trim Pieces_003.png ChessSet_01_bb.png
convert -trim Pieces_004.png ChessSet_01_bn.png
convert -trim Pieces_005.png ChessSet_01_bp.png
convert -trim Pieces_006.png ChessSet_01_wk.png
convert -trim Pieces_007.png ChessSet_01_wq.png
convert -trim Pieces_008.png ChessSet_01_wr.png
convert -trim Pieces_009.png ChessSet_01_wb.png
convert -trim Pieces_010.png ChessSet_01_wn.png
convert -trim Pieces_011.png ChessSet_01_wp.png
rm -f Pieces_*.png
