# DiLena board elements

## Author

These chess boards and pieces were created by Daniela Di Lena [http://www.dilena.de/]

She granted us kind permission to use these in our chess software as long as we give her credit about it.
